const express = require('express');
const path = require('path');
const got = require('got')
const moment = require('moment')

const app = express();
const port = process.env.PORT || 4000;

const parseResponse = (event, timesList, dateInput)  => {
  const parsedEvent = {
    date: {
      month: moment(dateInput).format('MMM'),
      date: moment(dateInput).format('DD'),
      day: moment(dateInput).format('ddd')
    },
    times: timesList,
    featuredImage: event.featuredImage || event.thumbnail,
    propertyName: event.propertyName,
    summary: event.summary,
    prices: event.prices,
    webUrl: event.webUrl,
    ticketsUrl: event.ticketsUrl,
    name: event.name
  }
  return parsedEvent
}

const parseList = async (eventList, jsTimestamp) => {
  let dateInput = moment.unix(parseInt(jsTimestamp)/1000)
  const matchingEvents = []
  // Time complexity O(ij), where i is how many events we have and j
  // is how many dates each of those events has
  for (let i = 0; i < eventList.length; i++) {

    // We check the event if it has at least one date in its object
    if (eventList[i].dates.length > 0 ) {
      let datesList = eventList[i].dates
      let timesList = []

      // We then check each date from the event for three conditions:
      // 1. The inputDate is inclusively between the running dates for the event
      // 2. The inputDate is in the running shows of the week (the days array)
      // 3. The inputDate is NOT included in the blackout list

      // If all conditions are satisfied, we add it to our timesList
      for (let j = 0; j < datesList.length; j++) {
        let blackoutList = datesList[j].blackouts || []
        if (dateInput.isBetween(datesList[j].start, datesList[j].end, null, [])
          && datesList[j].days.includes(dateInput.day())
          && !blackoutList.includes(dateInput.format('YYYY-MM-DD'))
        ) {
          timesList = timesList.concat(datesList[j].times)
        }
      }

      // If we have any times in our timesList, that implies that we have a possibe
      // time available for the dateInput passed in
      if (timesList.length > 0) {
        const parsedResponse = await parseResponse(eventList[i], timesList, dateInput)
        matchingEvents.push(parsedResponse)
      }
    }
  }

  return matchingEvents
}

// API calls
app.get('/api/hotel', async (req,res) => {
  try {
    const response = await got('https://www.caesars.com/api/v1/markets/lvm/events')
    const eventList = JSON.parse(response.body)
    const parsedList = await parseList(eventList, req.query.date)
    res.send(JSON.stringify(parsedList))
  } catch (err) {
    // In actual production, this error should be handled appropriately
    console.log(err.response.body);
  }
})

if (process.env.NODE_ENV === 'production') {
  // Serve any static files
  app.use(express.static(path.join(__dirname, 'client/build')));

  // Handle React routing, return all requests to React app
  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

app.listen(port, () => console.log(`Listening on port ${port}`));
