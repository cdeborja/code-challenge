# Caesars Coding Challenge

### Thank you
I would like to first say thank you for the opportunity to attempt this coding challenge. I thought it was a bit challenging at times, but I am proud of what I was able to produce.

### How to Run
(Make sure you are on the latest versions of npm and node)
Go to the root of the project and then run the script `npm run start-project`

This will install all dependencies in the root directory as well as the client directory. It will also start the react frontend on `localhost:3000` and the proxy server on `localhost:4000`. After the script completes, it will automatically open your web browser to `localhost:3000`.

### Issues
Working through the challenge I ran into a couple of issues:
* Some event responses from the api had no `featuredImage`
  * I resolved this on the frontend by having a fallback to the `thumbnail` image
* Some events did not have a `webUrl`
  * If an event did _not_ have a webUrl, I did not display the `Learn More` button on the frontend
* Some events did not have any `dates`
  * Events with no dates attached were parsed out in the proxy server by trying to loop through the list of dates for a specific event (if it was an empty array, it would skip over that event)

### To-dos
Given more time, I would have loved to fix that proxy server issue I was having when trying to deploy to codesandbox.io. Furthermore with more time, I would go back and try to condense more of the css and update more class names and functions appropriately.
