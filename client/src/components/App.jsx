import React, {Component} from 'react';
import moment from 'moment'
import DatePicker from 'react-datepicker'
import EventItem from './EventItem'
import PropertyPicker from './PropertyPicker'
import totalVegas from '../assets/total-vegas.png'

import '../styles/App.css';
import 'react-datepicker/dist/react-datepicker.css'

class App extends Component {
  constructor() {
    super()
    this.state = {
      displayedEventList: [],
      eventList: [],
      inputDate: null,
      loaded: false,
      propertyList: []
    }
    this.selectDate = this.selectDate.bind(this)
    this.updateProperty = this.updateProperty.bind(this)
    this.getPropertyList = this.getPropertyList.bind(this)
  }

  componentDidMount() {
    this.fetchEventList(moment())
  }

  fetchEventList(date) {

    fetch(`/api/hotel?date=${date}`).then((response) => {
      return response.json();
    }).then((res) => {
      this.setState({inputDate: date, eventList: res, displayedEventList: res, loaded: true, propertyList: this.getPropertyList(res)})
    }).catch(err => {
      // In production, probably would put a modal for catching errors
      console.log(err)
    });
  }

  displayEventList() {
    if (this.state.displayedEventList) {
      if (this.state.displayedEventList.length === 0) {
        return <p>No events available on selected date</p>
      }
      return this.state.displayedEventList.map((eventItem, idx) => {
        return <EventItem key={idx} event={eventItem}/>
      })
    } else {
      return <p>Finding you some great shows!</p>
    }
  }

  getPropertyList(eventList) {
    const propertyList = []
    eventList.forEach((event) => {
      if (propertyList.indexOf(event.propertyName) < 0) {
        propertyList.push(event.propertyName)
      }
    })
    return propertyList
  }

  selectDate(date) {
    this.fetchEventList(date)
  }

  updateProperty(e) {
    if (e.currentTarget.value === 'All') {
      // Redisplays all events
      this.setState({displayedEventList: this.state.eventList})
    } else {
      // Displays events of specific property
      const propertyTarget = e.currentTarget.value
      const matchingPropertyEvent = []

      for (let i = 0; i < this.state.eventList.length; i++) {
        if (this.state.eventList[i].propertyName === propertyTarget) {
          matchingPropertyEvent.push(this.state.eventList[i])
        }
      }
      this.setState({displayedEventList: matchingPropertyEvent})
    }
  }

  render() {

    if (!this.state.loaded) {
      return (<div className="App">
        <header className="App-header">
          <img src={totalVegas} alt="site-logo"/>
          <h1 className="App-title">Caesars Entertainment</h1>
          <p>Loading...</p>
        </header>
      </div>)
    } else {
      return (<div className="App">
        <header className="App-header">
          <img src={totalVegas} alt="site-logo"/>
          <h1 className="App-title">Caesars Entertainment</h1>
          <p>Enter date to start searching</p>
          <DatePicker selected={this.state.inputDate} onChange={this.selectDate} dateFormat="ddd MMM DD YYYY"/>
          <PropertyPicker propertyList={this.state.propertyList} updateProperty={this.updateProperty}/>
        </header>
        <div id='events-list'>
          {this.displayEventList()}
        </div>
        <p>
          ------
        </p>
      </div>);
    }
  }
}

export default App;
