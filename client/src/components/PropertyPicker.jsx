import React, {Component} from 'react'

class PropertyPicker extends Component {
  constructor(props) {
    super(props)
    this.updateProperty = this.props.updateProperty.bind(this)
  }

  displayPropertyList() {
    if (this.props.propertyList.length === 0) {
      return <option key={0} value='No Options'>No Options</option>
    }
    const options = [<option key={0} value='All'>All</option>]
    const propertyOptions = this.props.propertyList.map((propertyName, idx) => {
      return <option key={idx+1} value={propertyName}>{propertyName}</option>
    })
    return options.concat(propertyOptions)
  }
  render() {
    return <select onChange={this.updateProperty} id='property-selector'>
      {this.displayPropertyList()}
    </select>
  }
}

export default PropertyPicker
