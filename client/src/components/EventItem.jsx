import React, {Component} from 'react'
import '../styles/EventItem.css'

class EventItem extends Component {
  displayWebUrl() {
    if (!this.props.event.webUrl) {
      return
    } else {
      return <div>
        <a target='_blank' href={this.props.event.webUrl}>
          Learn More</a>
      </div>
    }
  }

  displayTime() {
    if (!this.props.event.times) {
      return <div>No times provided</div>
    } else {
      return <div className='event-times'>
        <div>
          <i className="far fa-clock"/>
          <span>{this.props.event.times.join(' ')}</span>
        </div>
        <div>
          {this.props.event.prices}
        </div>
      </div>
    }
  }

  displayInformation() {
    return <div className='event-information'>
      <div className='event-description'>
        <span>{this.props.event.date.month}&nbsp;{this.props.event.date.date}&nbsp;{this.props.event.date.day}</span>
        <span>{this.props.event.name}
        </span>
        <span>
          <i className="fas fa-map-marker-alt"/> {this.props.event.propertyName}
        </span>
        <span>{this.props.event.summary}</span>
      </div>
      <div className='event-more'>
        {this.displayTime()}
        <div className='event-links'>
          {this.displayWebUrl()}
          <div>
            <a target='_blank' href={this.props.event.ticketsUrl}>Find Tickets</a>
          </div>
        </div>
      </div>
    </div>
  }

  displayThumbnail() {
    if (!this.props.event.featuredImage) {
      return <div>NO IMAGE</div>
    } else {
      return <div className='event-thumbnail'>
        <div>
          <img alt='event-preview' src={`http://caesars.com${this.props.event.featuredImage.url}`}/>
        </div>
      </div>
    }
  }

  displayDate() {
    return <div className='event-date'>
      <span className='event-month'>{this.props.event.date.month}</span>
      <span className='event-month-day'>{this.props.event.date.date}</span>
      <span className='event-weekday'>{this.props.event.date.day}</span>
    </div>

  }

  render() {
    return <section className='event-container'>
      {this.displayDate()}
      {this.displayThumbnail()}
      {this.displayInformation()}
    </section>
  }
}

export default EventItem
